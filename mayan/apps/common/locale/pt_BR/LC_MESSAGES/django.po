# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aline Freitas <aline@alinefreitas.com.br>, 2016
# Emerson Soares <on.emersonsoares@gmail.com>, 2011
# Jadson Ribeiro <jadsonbr@outlook.com.br>, 2017
# Roberto Rosario, 2012
# Rogerio Falcone <rogerio@falconeit.com.br>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-26 22:22-0400\n"
"PO-Revision-Date: 2018-09-27 02:29+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:85 permissions_runtime.py:7 settings.py:12
msgid "Common"
msgstr "Comúm"

#: classes.py:214
msgid "Available attributes: \n"
msgstr ""

#: classes.py:254
msgid "Available fields: \n"
msgstr ""

#: dashboards.py:7
msgid "Main"
msgstr ""

#: forms.py:25
msgid "Selection"
msgstr "Seleção"

#: generics.py:137
#, python-format
msgid "Unable to transfer selection: %s."
msgstr "Não foi possível transferir a seleção: %s."

#: generics.py:161
msgid "Add"
msgstr "Adicionar"

#: generics.py:172
msgid "Remove"
msgstr "Remover"

#: generics.py:354
#, python-format
msgid "%(object)s not created, error: %(error)s"
msgstr "%(object)s não criado, erro: %(error)s"

#: generics.py:365
#, python-format
msgid "%(object)s created successfully."
msgstr "%(object)s criado com sucesso."

#: generics.py:394
#, python-format
msgid "%(object)s not deleted, error: %(error)s."
msgstr "%(object)s não removido, erro: %(error)s."

#: generics.py:405
#, python-format
msgid "%(object)s deleted successfully."
msgstr "%(object)s removido com sucesso."

#: generics.py:451
#, python-format
msgid "%(object)s not updated, error: %(error)s."
msgstr "%(object)s não atualizado, erro: %(error)s."

#: generics.py:462
#, python-format
msgid "%(object)s updated successfully."
msgstr "%(object)s atualizado com sucesso."

#: links.py:40
msgid "About this"
msgstr "Sobre isso"

#: links.py:43 views.py:63
msgid "Check for updates"
msgstr ""

#: links.py:47
msgid "User details"
msgstr "Detalhes do usuário"

#: links.py:51
msgid "Edit details"
msgstr "Editar Detalhes"

#: links.py:56
msgid "Locale profile"
msgstr "Perfil Idioma"

#: links.py:61
msgid "Edit locale profile"
msgstr "Editar perfil Idioma"

#: links.py:66
msgid "Documentation"
msgstr "Documentação"

#: links.py:70 links.py:81
msgid "Errors"
msgstr ""

#: links.py:75
msgid "Clear all"
msgstr ""

#: links.py:85
msgid "Forum"
msgstr "Fórum"

#: links.py:89 views.py:166
msgid "License"
msgstr "Licença"

#: links.py:92 views.py:241
msgid "Other packages licenses"
msgstr "Licenças de outros pacotes"

#: links.py:96
msgid "Setup"
msgstr "Configurações"

#: links.py:99
msgid "Source code"
msgstr "Código fonte"

#: links.py:103
msgid "Support"
msgstr "Suporte"

#: links.py:107 queues.py:10 views.py:284
msgid "Tools"
msgstr "Ferramentas"

#: literals.py:9
msgid ""
"Your database backend is set to use SQLite. SQLite should only be used for "
"development and testing, not for production."
msgstr ""

#: literals.py:18
msgid "Days"
msgstr "Dia"

#: literals.py:19
msgid "Hours"
msgstr "Hora"

#: literals.py:20
msgid "Minutes"
msgstr "Minutos"

#: management/commands/convertdb.py:28
msgid ""
"Restricts dumped data to the specified app_label or app_label.ModelName."
msgstr ""

#: management/commands/convertdb.py:35
msgid ""
"The database from which data will be exported. If omitted the database named"
" \"default\" will be used."
msgstr ""

#: management/commands/convertdb.py:42
msgid ""
"The database to which data will be imported. If omitted the database named "
"\"default\" will be used."
msgstr ""

#: management/commands/convertdb.py:49
msgid ""
"Force the conversion of the database even if the receiving database is not "
"empty."
msgstr ""

#: management/commands/installjavascript.py:15
msgid "Process a specific app."
msgstr ""

#: menus.py:15
msgid "System"
msgstr ""

#: menus.py:26 models.py:81
msgid "User"
msgstr "Usuário"

#: mixins.py:83
#, python-format
msgid "Operation performed on %(count)d object"
msgstr "Operação executada em %(count)d objeto"

#: mixins.py:84
#, python-format
msgid "Operation performed on %(count)d objects"
msgstr "Operação executada em %(count)d objetos"

#: mixins.py:261
msgid "Object"
msgstr "Objeto"

#: models.py:24
msgid "Namespace"
msgstr "namespace"

#: models.py:35 models.py:55
msgid "Date time"
msgstr "Data e hora"

#: models.py:37 views.py:211
msgid "Result"
msgstr "resultado"

#: models.py:43
msgid "Error log entry"
msgstr ""

#: models.py:44
msgid "Error log entries"
msgstr ""

#: models.py:51
msgid "File"
msgstr "Arquivo"

#: models.py:53
msgid "Filename"
msgstr "Nome do arquivo"

#: models.py:59
msgid "Shared uploaded file"
msgstr "Arquivo carregado compartilhado"

#: models.py:60
msgid "Shared uploaded files"
msgstr "Arquivos carregados compartilhados "

#: models.py:85
msgid "Timezone"
msgstr "Fuso horário"

#: models.py:88
msgid "Language"
msgstr "Linguagem"

#: models.py:94
msgid "User locale profile"
msgstr "Perfil de localidade do usuário"

#: models.py:95
msgid "User locale profiles"
msgstr "Perfis de localidade do usuário"

#: permissions_runtime.py:10
msgid "View error log"
msgstr ""

#: queues.py:8
msgid "Default"
msgstr "Padrão"

#: queues.py:12
msgid "Common periodic"
msgstr ""

#: queues.py:16
msgid "Delete stale uploads"
msgstr ""

#: settings.py:17
msgid "Automatically enable logging to all apps."
msgstr "Ativar automaticamente o registro de todos os aplicativos."

#: settings.py:23
msgid ""
"Time to delay background tasks that depend on a database commit to "
"propagate."
msgstr "Tempo para atrasar as tarefas de fundo que dependem da propagação de informação na base de dados."

#: settings.py:31
msgid "An integer specifying how many objects should be displayed per page."
msgstr "Um número inteiro que especifica quantos objetos se deve mostrar por página."

#: settings.py:38
msgid "Enable error logging outside of the system error logging capabilities."
msgstr ""

#: settings.py:45
msgid "Path to the logfile that will track errors during production."
msgstr ""

#: settings.py:52
msgid "Name to be displayed in the main menu."
msgstr ""

#: settings.py:58
msgid "A storage backend that all workers can use to share files."
msgstr "Um suporte de armazenamento que todos os trabalhadores podem usar para compartilhar arquivos."

#: settings.py:69
msgid ""
"Temporary directory used site wide to store thumbnails, previews and "
"temporary files."
msgstr "Pasta temporária utilizada em todo o site para armazenar imagens em miniatura, visualizações e arquivos temporários."

#: settings.py:75
msgid "Django"
msgstr ""

#: settings.py:80
msgid ""
"A list of strings representing the host/domain names that this site can "
"serve. This is a security measure to prevent HTTP Host header attacks, which"
" are possible even under many seemingly-safe web server configurations. "
"Values in this list can be fully qualified names (e.g. 'www.example.com'), "
"in which case they will be matched against the request's Host header exactly"
" (case-insensitive, not including port). A value beginning with a period can"
" be used as a subdomain wildcard: '.example.com' will match example.com, "
"www.example.com, and any other subdomain of example.com. A value of '*' will"
" match anything; in this case you are responsible to provide your own "
"validation of the Host header (perhaps in a middleware; if so this "
"middleware must be listed first in MIDDLEWARE)."
msgstr ""

#: settings.py:98
msgid ""
"When set to True, if the request URL does not match any of the patterns in "
"the URLconf and it doesn't end in a slash, an HTTP redirect is issued to the"
" same URL with a slash appended. Note that the redirect may cause any data "
"submitted in a POST request to be lost. The APPEND_SLASH setting is only "
"used if CommonMiddleware is installed (see Middleware). See also "
"PREPEND_WWW."
msgstr ""

#: settings.py:110
msgid ""
"A dictionary containing the settings for all databases to be used with "
"Django. It is a nested dictionary whose contents map a database alias to a "
"dictionary containing the options for an individual database. The DATABASES "
"setting must configure a default database; any number of additional "
"databases may also be specified."
msgstr ""

#: settings.py:122
msgid ""
"Default: 2621440 (i.e. 2.5 MB). The maximum size in bytes that a request "
"body may be before a SuspiciousOperation (RequestDataTooBig) is raised. The "
"check is done when accessing request.body or request.POST and is calculated "
"against the total request size excluding any file upload data. You can set "
"this to None to disable the check. Applications that are expected to receive"
" unusually large form posts should tune this setting. The amount of request "
"data is correlated to the amount of memory needed to process the request and"
" populate the GET and POST dictionaries. Large requests could be used as a "
"denial-of-service attack vector if left unchecked. Since web servers don't "
"typically perform deep request inspection, it's not possible to perform a "
"similar check at that level. See also FILE_UPLOAD_MAX_MEMORY_SIZE."
msgstr ""

#: settings.py:142
msgid ""
"Default: [] (Empty list). List of compiled regular expression objects "
"representing User-Agent strings that are not allowed to visit any page, "
"systemwide. Use this for bad robots/crawlers. This is only used if "
"CommonMiddleware is installed (see Middleware)."
msgstr ""

#: settings.py:153
msgid ""
"Default: 'django.core.mail.backends.smtp.EmailBackend'. The backend to use "
"for sending emails."
msgstr ""

#: settings.py:161
msgid "Default: 'localhost'. The host to use for sending email."
msgstr ""

#: settings.py:168
msgid ""
"Default: '' (Empty string). Password to use for the SMTP server defined in "
"EMAIL_HOST. This setting is used in conjunction with EMAIL_HOST_USER when "
"authenticating to the SMTP server. If either of these settings is empty, "
"Django won't attempt authentication."
msgstr ""

#: settings.py:179
msgid ""
"Default: '' (Empty string). Username to use for the SMTP server defined in "
"EMAIL_HOST. If empty, Django won't attempt authentication."
msgstr ""

#: settings.py:188
msgid "Default: 25. Port to use for the SMTP server defined in EMAIL_HOST."
msgstr ""

#: settings.py:195
msgid ""
"Default: False. Whether to use a TLS (secure) connection when talking to the"
" SMTP server. This is used for explicit TLS connections, generally on port "
"587. If you are experiencing hanging connections, see the implicit TLS "
"setting EMAIL_USE_SSL."
msgstr ""

#: settings.py:205
msgid ""
"Default: False. Whether to use an implicit TLS (secure) connection when "
"talking to the SMTP server. In most email documentation this type of TLS "
"connection is referred to as SSL. It is generally used on port 465. If you "
"are experiencing problems, see the explicit TLS setting EMAIL_USE_TLS. Note "
"that EMAIL_USE_TLS/EMAIL_USE_SSL are mutually exclusive, so only set one of "
"those settings to True."
msgstr ""

#: settings.py:217
msgid ""
"Default: None. Specifies a timeout in seconds for blocking operations like "
"the connection attempt."
msgstr ""

#: settings.py:225
msgid ""
"Default: 2621440 (i.e. 2.5 MB). The maximum size (in bytes) that an upload "
"will be before it gets streamed to the file system. See Managing files for "
"details. See also DATA_UPLOAD_MAX_MEMORY_SIZE."
msgstr ""

#: settings.py:236
msgid "Name of the view attached to the branch anchor in the main menu."
msgstr ""

#: settings.py:243
msgid ""
"A list of strings designating all applications that are enabled in this "
"Django installation. Each string should be a dotted Python path to: an "
"application configuration class (preferred), or a package containing an "
"application."
msgstr ""

#: settings.py:253
msgid ""
"Default: '/accounts/login/' The URL where requests are redirected for login,"
" especially when using the login_required() decorator. This setting also "
"accepts named URL patterns which can be used to reduce configuration "
"duplication since you don't have to define the URL in two places (settings "
"and URLconf)."
msgstr ""

#: settings.py:265
msgid ""
"Default: '/accounts/profile/' The URL where requests are redirected after "
"login when the contrib.auth.login view gets no next parameter. This is used "
"by the login_required() decorator, for example. This setting also accepts "
"named URL patterns which can be used to reduce configuration duplication "
"since you don't have to define the URL in two places (settings and URLconf)."
msgstr ""

#: settings.py:274
msgid "Celery"
msgstr ""

#: settings.py:279
msgid ""
"Default: \"amqp://\". Default broker URL. This must be a URL in the form of:"
" transport://userid:password@hostname:port/virtual_host Only the scheme part"
" (transport://) is required, the rest is optional, and defaults to the "
"specific transports default values."
msgstr ""

#: settings.py:289
msgid ""
"Default: No result backend enabled by default. The backend used to store "
"task results (tombstones). Refer to "
"http://docs.celeryproject.org/en/v4.1.0/userguide/configuration.html#result-"
"backend"
msgstr ""

#: utils.py:74
msgid "Anonymous"
msgstr "Anônimo"

#: validators.py:29
msgid ""
"Enter a valid 'internal name' consisting of letters, numbers, and "
"underscores."
msgstr ""

#: views.py:39
msgid "About"
msgstr "Sobre"

#: views.py:51
#, python-format
msgid "The version you are using is outdated. The latest version is %s"
msgstr ""

#: views.py:56
msgid "It is not possible to determine the latest version available."
msgstr ""

#: views.py:60
msgid "Your version is up-to-date."
msgstr ""

#: views.py:77
msgid "Current user details"
msgstr "Detalhes do usuário atual"

#: views.py:82
msgid "Edit current user details"
msgstr "Editar detalhes do usuário atual"

#: views.py:102
msgid "Current user locale profile details"
msgstr "Detalhes do perfil de localidade do usuário atual"

#: views.py:109
msgid "Edit current user locale profile details"
msgstr "Editar detalhes do perfil de localização do usuário atual"

#: views.py:157
msgid "Dashboard"
msgstr "Painel de controle"

#: views.py:175
#, python-format
msgid "Clear error log entries for: %s"
msgstr ""

#: views.py:192
msgid "Object error log cleared successfully"
msgstr ""

#: views.py:210
msgid "Date and time"
msgstr "Data e hora"

#: views.py:215
#, python-format
msgid "Error log entries for: %s"
msgstr ""

#: views.py:260
msgid "No setup options available."
msgstr ""

#: views.py:262
msgid ""
"No results here means that don't have the required permissions to perform "
"administrative task."
msgstr ""

#: views.py:266
msgid "Setup items"
msgstr "Itens da Configuração"

#: views.py:310
msgid "No action selected."
msgstr "Nenhuma ação selecionada."

#: views.py:318
msgid "Must select at least one item."
msgstr "Deve selecionar pelo menos um item."
